var express =  require('express');
var cors =  require('cors');
var path =  require('path');

const app        = express(),
    DIST_DIR     = path.resolve(__dirname, 'dist')
    HTML_FILE    = path.resolve(__dirname, './dist/index1.html'),
    DEFAULT_PORT = 5000;

app.use(cors());
app.set('port', (process.env.PORT || DEFAULT_PORT));
app.use(express.static(DIST_DIR));
app.get('/', function(res, req) { res.sendFile(HTML_FILE) } );

app.listen(app.get('port'), function(err) {
    console.log('Route Map currrently running..');
});