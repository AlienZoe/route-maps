var path = require('path'),
    webpack = require('webpack'),
    constant = require('./constant');
    ExtractTextWebpackPlugin = require('extract-text-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    devtool: 'source-map',
    entry: {
        app: './src/app/bootstrap.js'
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin(),
        new ExtractTextWebpackPlugin('styles-[contentHash:12].css'),
        new HtmlWebpackPlugin({
            title: 'Route Map || Airlinemetrics',
            template: 'index-template.html',
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removedRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true
            },
        }),
        new webpack.DefinePlugin({
            API_KEY: JSON.stringify('AIzaSyC-kzHQXUITqd6nIcp_RuIst_aqxImrE0A'),
            ASSET: JSON.stringify(constant.ASSET_PATH),
            LIB: JSON.stringify(constant.LIB_PATH),
            ROOT: JSON.stringify(constant.ROOT_PATH)
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: '/node_modules/',
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets:
                        [["latest", { "modules": false }]
                        ]
                    }
                }
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        name: 'images/[hash:12].[ext]',
                    }
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextWebpackPlugin.extract({
                    use: [{
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            localIdentName: '[hash:base64:12]'
                        }
                    },
                        'sass-loader'
                    ]
                })
            },
            {
                test: /\.html$/,
                use: [{
                    loader: 'html-loader',
                    options: {
                        minimize: true,
                        removeComments: false,
                        collapseWhitespace: false
                    }
                }],
            }
        ]
    },
    output: {
        path: path.resolve(__dirname, '../dist'),
        publicPath: '/',
        filename: '[name][hash:12].min.js'
    },
    resolve: {
        alias: {
            globalcss: constant.ASSET_PATH + 'scss/global.scss'
        },
        extensions: ['.js', '.html', '.scss']
    }
}