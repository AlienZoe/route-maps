var path = require('path');
var ROOT = path.resolve(__dirname, '..');
var root = path.join.bind(path, ROOT);

module.exports = {
    ROOT_PATH    :    ROOT,
    DIST_PATH    :    root('dist'),
    APP_PATH     :    root('app'),
    ASSET_PATH   :    root('src/assets'),
    LIB_PATH     :    root('src/lib'),
}