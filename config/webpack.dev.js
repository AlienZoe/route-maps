var path = require('path'),
    webpack = require('webpack'),
    constant = require('./constant');

module.exports = {
    // externals: {
    //     'angular': 'angular'
    // },
    devtool: 'inline-source-map',
    context: path.resolve('src'),
    entry: {
        app: [
            './app/bootstrap.js',
            'webpack/hot/dev-server',
            'webpack-dev-server/client?http://localhost:8080'
        ],
        // vendors: [
        //     'angular'
        // ]
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: '/node_modules/',
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ["env", { "modules": false }]
                        ]
                    }
                }
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        name: 'images/[hash:12].[ext]',
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            sourceMap: true,
                            localIdentName: '[path][name]---[local]'
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            modules: true,
                            sourceMap: true,
                        }
                    }
                ]
            },
            {
                test: /\.html$/,
                use: [{
                    loader: 'html-loader',
                    options: {
                        minimize: true,
                        removeComments: false,
                        collapseWhitespace: false
                    }
                }],
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            API_KEY: JSON.stringify('AIzaSyC-kzHQXUITqd6nIcp_RuIst_aqxImrE0A'),
            ASSET: JSON.stringify(constant.ASSET_PATH),
            LIB: JSON.stringify(constant.LIB_PATH),
            ROOT: JSON.stringify(constant.ROOT_PATH)
        })
    ],
    output: {
        path: constant.DIST_PATH,
        publicPath: '/dist/',
        filename: '[name].js'
    },
    resolve: {
        alias: {
            globalcss: constant.ASSET_PATH + 'scss/global.scss'
        },
        extensions: ['.js', '.html', '.scss']
    }
}