var WebpackDevServer = require('webpack-dev-server'),
    webpack          = require('webpack'),
    config           = require('../webpack.config.js'),
    path             = require('path'),

    compiler         = webpack(config),
    server           = new WebpackDevServer(compiler, {
        hot: true,
        filename:   config.output.filename,
        publicPath: config.output.publicPath,
        stats: {
            colors: true
        }
    });

server.listen(8080, 'localhost', function() {});
