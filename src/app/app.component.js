/**
* Name: App Component Template
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
export const AppComponent = {
    template: `
        <ui-view></ui-view>
    `
}