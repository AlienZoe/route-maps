/**
* Name: Services
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
const HTTP = new WeakMap();

class Service {
	constructor($http) {
		HTTP.set(this, $http);
	}

	loadOriginDestination() {
		return HTTP.get(this).get('/public/country.json').then(result => result.data);
	}

	loadHub() {
		return HTTP.get(this).get('/public/hub.json').then(result => result.data);
	}
}

Service.$inject = [ '$http' ];
export default Service;