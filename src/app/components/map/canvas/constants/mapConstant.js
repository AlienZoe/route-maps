/**
* Name: Map Constants
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
export const DEFAULTS = {
    zoom: 3,
    minZoom: 2,
    maxZoom: 7,
    noClear: true,
    disableDefaultUI: true,
    mapTypeId: google.maps.MapTypeId.TERRAIN,
    styles: [{
        "featureType": "administrative",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "landscape",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "transit",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "poi",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "road",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "water",
        "stylers": [{
            "color": "#9ed5ed"
        }]
    }]
}

export const CONFIGS = {
    LIMIT_NORTH: 85,
    LIMIT_SOUTH: -85,
}
