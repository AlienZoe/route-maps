/**
* Name: Location Markers
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
class LocationMarker extends google.maps.Polyline {
    constructor(mapClass, coordinates, name) {
        super();
        this.setOptions(this.POLYLINE_OPTIONS);
        this.setPath([coordinates, coordinates]);
        this.setMap(mapClass);

        this.set('zIndex', this.ICON_OPTIONS.DEFAULT.ZINDEX);
        this._setIcon(this.ICON_OPTIONS);
        this._getEventListeners();
    }

    _getEventListeners() {
        google.maps.event.addListener(this, 'mouseover', this._onMouseOver);
    }

    _onMouseOver() {

    }

    _setIcon(icon) {
        let DOT = {
            icon: {
                path: icon.DEFAULT.PATH,
                strokeOpacity: 1,
                strokeWeight: 2,
                strokeColor: icon.PROPERTIES.COLOR_STROKE,
                fillColor: icon.PROPERTIES.COLOR_DEFAULT,
                fillOpacity: 1,
                scale: 5
            }
        },
            SHADOW = {
                icon: {
                    path: icon.DEFAULT.PATH,
                    fillColor: icon.PROPERTIES.COLOR_SHADOW,
                    fillOpacity: icon.PROPERTIES.OPACITY_SHADOW,
                    scale: 6
                }
            };

        this._icon = [SHADOW, DOT];
        this.set('icons', this._icon);
    }

    get POLYLINE_OPTIONS() {
        return {
            strokeOpacity: 0,
            strokeWeight: 12,
            zIndex: 1
        }
    }

    get ICON_OPTIONS() {
        return {
            DEFAULT: {
                PATH: google.maps.SymbolPath.CIRCLE,
                ZINDEX: 1
            },
            PROPERTIES: {
                COLOR_DEFAULT: '#4455B6',
                COLOR_STROKE: '#ffffff',
                COLOR_SHADOW: '#000000',
                OPACITY_SHADOW: '0.1',
            }
        }
    }
}

export default class LocationMarkers {
    constructor(mapClass) {
        this._mapClass = mapClass
    }

    add(_models) {
        angular.forEach(_models, (val, key) => {
            let latLng = val.latlng;
            let name = val.airport;

            return new LocationMarker(this._mapClass, latLng, name);
        });
    }
}