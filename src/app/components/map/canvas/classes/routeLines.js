/**
* Name: Routes Line
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
class Node {
    constructor() {
        this.links = [];
    }
}

class Link {
    constructor(fromModel, toModel, line, nextNode) {
        this.legModel = [fromModel, toModel, line];
        this.nextNode = nextNode;
    }
}

class RouteLine extends google.maps.Polyline {
    constructor(mapClass, lngLatFrom, lngLatTo, line) {
        super();
        let self = this;

        this._lngLatFrom = lngLatFrom;
        this._lngLatTo = lngLatTo;
        this._tweenInstance = null;
        this._animateCallback = null;
        this._oneLine = line;

        this._setOptions();
        // this._setPath();
        // this.setOptions(this.POLYLINE_OPTIONS);
        // this.setPath([lngLatFrom, lngLatTo]);

        if (!this._oneLine)
            this._setIcons(this.ICON_OPTIONS);
        this.setMap(mapClass);
    }

    animateLine(callback) {
        let GoogleMaps = google.maps;
        let that, animateTime, animatePath,
            animateObj = { position: 0 };

        that = this;
        that._animateCallback = callback;
        animateTime = GoogleMaps.geometry.spherical.computeDistanceBetween(that._lngLatFrom, that._lngLatTo) / (5000 * 1000);
        animatePath = new GoogleMaps.MVCArray([that._lngLatFrom]);

        this.setPath(animatePath); //Start Line

        this._tweenInstance = TweenLite.to(animateObj, animateTime, {
            position: 1,
            ease: Linear.easeNone,
            onUpdate: () => {
                let interpolation = GoogleMaps.geometry.spherical.interpolate(that._lngLatFrom, that._lngLatTo, animateObj.position);

                animatePath.push(interpolation);
            },
            onComplete: () => {
                that._triggerCallback();
                that._setPath();
            }
        });
    }

    _triggerCallback() {
        if (this._animateCallback)
            this._animateCallback();
    }

    _killTween() {
        if (this._tweenInstance) {
            this._tweenInstance.kill();
            this._tweenInstance = null;
        }
    }

    remove() {
        this.setMap(null);
    }

    _setOptions() {
        this.setOptions(this.POLYLINE_OPTIONS);
    }

    _setPath() {
        this.setPath([
            this._lngLatFrom,
            this._lngLatTo
        ]);
    }

    _setIcons(icon) {
        let dash = {
            icon: {
                path: icon.DEFAULT.PATH,
                scale: icon.DEFAULT.SCALE,
                strokeOpacity: icon.PROPERTIES.OPACITY_STROKE
            },
            repeat: '5px'
        }

        this._icons = [dash];
        this.set('icons', this._icons);
    }

    get POLYLINE_OPTIONS() {
        return {
            strokeColor: this._oneLine ? "#FF0000" : "#008000",
            strokeOpacity: this._oneLine ? 1 : 0,
            strokeWeight: 1.1,
            geodesic: true,
        }
    }

    get ICON_OPTIONS() {
        return {
            DEFAULT: {
                PATH: 'M 0,-1 0,1',
                SCALE: 1
            },
            PROPERTIES: {
                OPACITY_STROKE: 1
            }
        }
    }
}

export default class RouteLines {
    constructor(mapClass) {
        this._mapClass = mapClass;
        this._routes = [];

    }

    createRouteLines(routeModels) {
        return new Promise((resolve, reject) => {
            this._clearRoutes();

            //Create a Link each Nodes
            let baseNode = null;
            let currentNode = null;

            baseNode = new Node();
            for (let key in routeModels) {
                let whatLine = (key === 'selected') ? true : false;

                angular.forEach(routeModels[key], (val2, key2) => {
                    currentNode = baseNode;

                    let ctr = 0;
                    let FROM, TO;
                    angular.forEach(val2, (val3, key3) => {
                        if (ctr % 2 === 0)
                            FROM = val3;

                        if (ctr % 2 === 1) {
                            TO = val3;

                            let node = new Node();
                            let link = new Link(FROM, TO, whatLine, node);

                            currentNode.links.push(link);
                            currentNode = node;
                            FROM = TO;
                            ctr++;
                        }
                        ctr++;
                    });
                });
            }
            resolve(baseNode);
        });
    }

    createLinks(options) {
        return new Promise((resolve, reject) => {
            let nodes = options.nodes;
            let mapClass = options.map;
            let animateLinks = options.links; //Functions
            let routes = [];

            animateLinks(nodes, mapClass, animateLinks, routes);
            resolve(routes);
        });
    }

    _animateLinks(nodes, mapClass, animateLinks, routes) {
        if (nodes.links.length !== 0) {
            angular.forEach(nodes.links, (val, key) => {
                let lngLatFrom = new google.maps.LatLng(val.legModel[0]),
                    lngLatTo = new google.maps.LatLng(val.legModel[1]),
                    line = val.legModel[2],

                    route = new RouteLine(mapClass, lngLatFrom, lngLatTo, line);

                route.animateLine(() => {
                    routes.push(route);
                    animateLinks(val.nextNode, mapClass, animateLinks, routes);
                });
            });
        }
    }

    _clearRoutes() {
        if (this._routes.length > 0)
            angular.forEach(this._routes, (routeLine, key) => {
                routeLine.remove();
            });
    }
}

// let fromLngLat = new google.maps.LatLng({ lat: 14.599512, lng: 120.984219 });
            // let toLngLat = new google.maps.LatLng({ lat: 25.2048, lng: 55.2708 });;
            // let test = new RouteLine(this._mapClass, fromLngLat, toLngLat);
            // test.animateLine();