/**
* Name: Label Overlay
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/

import Overlay from './overlay';

export default class LabelOverlay extends Overlay {
    constructor(mapClass) {
        super(mapClass);

    }

    _createDOM() {
        this.HTML = "<span>Manila</span>";
    }

}