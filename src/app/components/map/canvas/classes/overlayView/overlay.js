/**
* Name: Overlay
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
import MapClass from '../mapClass';
const OVERLAY = new WeakMap();

class DomOverlay extends google.maps.OverlayView {
    constructor(mapClass) {
        super();
        this.container = $("<div></div>");
        this.canDraw = false;
        this.setMap(mapClass);
    }

    onAdd() {
        let pane = $(this._Pane);
        this.container.appendTo(pane);
    }

    draw() {
        if (this.canDraw) {
            this._overlayPanes();
            console.log('I can Draw');
        }
    }

    appendDraw() {
        this.canDraw = true;
        this.draw();
        this.container.css("display", "block");
    }

    get _Pane() {
        return this.getPanes().mapPane;
    }

    get _Projection() {
        return this.getProjection();
    }

    _overlayPanes() {
        let projection = this._Projection;
        if (!projection) return;

        let child = this.container[0],
            parent = child.parentNode;
        console.log(child);
        console.log(parent);
        // if (parent) parent.removeChild(child);
        // console.log(child);
        // console.log(parent);
        // this._forEachOverlayGroup(function (overlayGroup) {
        //     if (overlayGroup)
        //         this._positionOverlayGroup(overlayGroup, projection);
        // }, this);

        // if (parent)
        //     parent.appendChild(child);

    }

    _overlayPane(overlayGroup, projection) {
        // let latLng = this._getModelForLatLng(overlayGroup.model).get("latLng"),
        //     position = projection.fromLatLngToDivPixel(latLng);

        // overlayGroup.el.style.left = position.x + "px";
        // overlayGroup.el.style.top = position.y + "px";

        // this._OverlayGroups(projection, overlayGroup, position);

    }
}

export default class Overlay extends DomOverlay {
    constructor(mapClass) {
        super(mapClass);

        this.HTML = "asd";
    }

    _OverlayGroups() {

    }
}