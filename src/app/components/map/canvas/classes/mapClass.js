/**
* Name: Map Class Override
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
import { DEFAULTS, CONFIGS } from '../constants/mapConstant';

export default class MapClass extends google.maps.Map {
    constructor() {
        const el = document.getElementById('map-canvas')
        super(el, DEFAULTS);
        this.init();
    }

    /**
    * Initialize The Map
    * @return void
    */
    init() {
        this.setCenter(new google.maps.LatLng(25.271139, 55.307485));
        this._createOverlay();

        // AddListener for EVENTS { TILESLOADED, CENTER CHANGED, ZOOM CHANGED}
        google.maps.event.addListenerOnce(this, 'tilesloaded', () => {
            google.maps.event.trigger(this, 'ready');
            this._checkPanBounds();
        });
        google.maps.event.addListener(this, 'center_changed', this._checkPanBounds);
        google.maps.event.addListener(this, 'zoom_changed', this._checkPanBounds);
    }

    /**
    * Create a empty overlay in the map
    * @return void
    */
    _createOverlay() {
        this.overlay = new google.maps.OverlayView();
        this.overlay.draw = function () { };
        this.overlay.setMap(this);
    }

    /**
    * Check the bound when pan the viewport
    * @return void
    */
    _checkPanBounds() {
        let currentBounds = this.getBounds(),
            northPosition = currentBounds.getNorthEast(),
            southPosition = currentBounds.getSouthWest();

        if (southPosition.lat() < CONFIGS.LIMIT_SOUTH) this._resetCenter(southPosition, CONFIGS.LIMIT_SOUTH);
        else if (northPosition.lat() > CONFIGS.LIMIT_NORTH) this._resetCenter(northPosition, CONFIGS.LIMIT_NORTH);
    }

    /**
    * Call when the Event Triggered
    * Limit the north and south bound when pannning.
    * @param { object } currentPosition
    * @param { int } latLimit
    * @return void
    */
    _resetCenter(currentPosition, latLimit) {
        let projection = this.overlay.getProjection();

        let limitLatLng = new google.maps.LatLng(latLimit, 0),
            limitY = projection.fromLatLngToDivPixel(limitLatLng).y,
            currentY = projection.fromLatLngToDivPixel(currentPosition).y;

        this.panBy(0, limitY - currentY);
    }
}
