/**
* Name: Map Group
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
import MapClass from './mapClass';
import LocationOverlay from './overlayView/locationOverlay';
import LabelOverlay from './overlayView/labelOverlay';
import LocationMarkers from './locationMarkers';
import RouteLines from './routeLines';

const ROUTELINE = new WeakMap();
export default class MapGroup {
    constructor(models) {
        this._models = models;

        //Instansiate different Classes
        this.mapClass = new MapClass();
        this.marker = new LocationMarkers(this.mapClass);
        ROUTELINE.set(this, new RouteLines(this.mapClass));

        // this.locationOverlay = new LocationOverlay(this.mapClass);
        // this.countryLabel = new LabelOverlay(this.mapClass);

        //Initialize the Whole Map
        this.initializeMap();
        this.markerListeners();
    }

    initializeMap() {
        // this.locationOverlay.appendDraw();
        // this.countryLabel.appendDraw();
        this._addMarkers;
    }

    markerListeners() {

    }

    /**
     * Draw a markers
     */
    get _addMarkers() {
        return this.marker.add(this._models);
    }

    /**
     * Draw a Links
     */
    set showRouteLine(collections) {
        let ME = ROUTELINE.get(this);
        ME.createRouteLines(collections)
            .then((res) => ME.createLinks({
                nodes: res,
                map: this.mapClass,
                links: ME._animateLinks
            }))
            .then((res) => {
                ME._routes = res;
            })
            .catch((e) => console.log(e));
    }
}