/**
* Name: Canvas Component
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
import './canvas.scss';
import template from './canvas';
import MapGroup from './classes/mapGroup';



class Controller {
    constructor($mdSidenav, $timeout) {
        const self = this;

        self.origin = '';
        self.destination = '';
        self.hub = '';
        self.routes = [];
        self.hubsList = [];
        

        const models = [
            {
                "id": "1",
                "airport": "Bangkok",
                "airport_code": "BKK",
                "country": "Thailand",
                "latlng": {
                    "lat": 15.8700,
                    "lng": 100.9925
                }
            },
            {
                "id": "2",
                "airport": "Dubai",
                "airport_code": "DXB",
                "country": "UAE",
                "latlng": {
                    "lat": 25.2048,
                    "lng": 55.2708
                }
            },
            {
                "id": "3",
                "airport": "Hong Kong",
                "airport_code": "HKG",
                "country": "Hong Kong",
                "latlng": {
                    "lat": 22.3964,
                    "lng": 114.1095
                }
            },
            {
                "id": "4",
                "airport": "Hyderabad",
                "airport_code": "HYD",
                "country": "India",
                "latlng": {
                    "lat": 17.3850,
                    "lng": 78.4867
                }
            },
            {
                "id": "5",
                "airport": "Manila",
                "airport_code": "MNL",
                "country": "Philippines",
                "latlng": {
                    "lat": 14.599512,
                    "lng": 120.984219
                }
            },
            {
                "id": "6",
                "airport": "Mati",
                "airport_code": "DVO",
                "country": "Philippines",
                "latlng": {
                    "lat": 6.9438,
                    "lng": 126.2467
                }
            },
            {
                "id": "7",
                "airport": "Melbourne",
                "airport_code": "MEL",
                "country": "Australia",
                "latlng": {
                    "lat": -37.813628,
                    "lng": 144.963058
                }
            },
            {
                "id": "8",
                "airport": "Nanning",
                "airport_code": "NNG",
                "country": "China",
                "latlng": {
                    "lat": 22.6096,
                    "lng": 108.1717
                }
            },
            {
                "id": "9",
                "airport": "Narita",
                "airport_code": "NRT",
                "country": "Japan",
                "latlng": {
                    "lat": 42.7926,
                    "lng": 141.6705
                }
            },
            {
                "id": "10",
                "airport": "Singapore",
                "airport_code": "SIN",
                "country": "Singapore",
                "latlng": {
                    "lat": 1.352083,
                    "lng": 103.819836
                }
            }
        ];

        self.mapGroup = new MapGroup(models); //Instansiate the Configuration for Map.
    }
}

export const MapCanvas = {
    template,
    controller: ['$mdSidenav', '$timeout', Controller],
    controllerAs: 'vm',
};