/**
* Name: Map Module
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
import { MapCanvas } from './canvas/canvas.component';
import { MapRightPanel } from './right-panel/right-panel.component';
import { MapLeftPanel } from './left-panel/left-panel.component';
import { config } from './config';

import MapService from './service';

const moduleName = 'map';

angular.module(moduleName, [])
    .service('mapService', MapService)
    .component('mapCanvas', MapCanvas)
    .component('mapRightPanel', MapRightPanel)
    .component('mapLeftPanel', MapLeftPanel)
    .config(['$uiRouterProvider', '$locationProvider', config])
    .name;

export default moduleName;