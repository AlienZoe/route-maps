/**
* Name: Left-Panel Component
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
import './left-panel.scss';
import template from './left-panel';

const SIDENAV = new WeakMap();
const TIMEOUT = new WeakMap();

class Controller {
    constructor($timeout, $mdSidenav) {
        SIDENAV.set(this, $mdSidenav);
        TIMEOUT.set(this, $timeout);
    }

    $onInit() {
        this.leftOpen = true;
    }

    toggleLeft() {
        SIDENAV.get(this)('left-nav').toggle();
        this.leftOpen = SIDENAV.get(this)('left-nav').isOpen();
        this.showLeft = false;

        if (!this.leftOpen) {
            TIMEOUT.get(this)(() => {
                this.showLeft = true;
            }, 1000);
        }
    }
}

export const MapLeftPanel = {
    template,
    require: {
        MAP: '^mapCanvas'
    },
    controller: ['$timeout', '$mdSidenav', Controller],
    controllerAs: 'vm',
    transclude: true
};