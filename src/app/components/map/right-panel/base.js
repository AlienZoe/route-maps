/**
* Name: Base Controller
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
const Q = new WeakMap();
const TIMEOUT = new WeakMap();
const SERVICE = new WeakMap();

export default class Base {
    constructor($q, $timeout, mapService) {
        Q.set(this, $q);
        TIMEOUT.set(this, $timeout);
        SERVICE.set(this, mapService);
    }

    loadAll() {
        // return SERVICE.get(this).loadOriginDestination().then(results => {
        //     return results.map(res => {
        //         return {
        //             value: res.latlng,
        //             name: res.airport + ' ' + res.airport_code,
        //             airport: res.airport,
        //             airport_code: res.airport_code,
        //             country: res.country,
        //             search: res.airport_code.toLowerCase()
        //         }
        //     });
        // });
        const results = [
            {
                "id": "1",
                "airport": "Bangkok",
                "airport_code": "BKK",
                "country": "Thailand",
                "latlng": {
                    "lat": 15.8700,
                    "lng": 100.9925
                }
            },
            {
                "id": "2",
                "airport": "Dubai",
                "airport_code": "DXB",
                "country": "UAE",
                "latlng": {
                    "lat": 25.2048,
                    "lng": 55.2708
                }
            },
            {
                "id": "3",
                "airport": "Hong Kong",
                "airport_code": "HKG",
                "country": "Hong Kong",
                "latlng": {
                    "lat": 22.3964,
                    "lng": 114.1095
                }
            },
            {
                "id": "4",
                "airport": "Hyderabad",
                "airport_code": "HYD",
                "country": "India",
                "latlng": {
                    "lat": 17.3850,
                    "lng": 78.4867
                }
            },
            {
                "id": "5",
                "airport": "Manila",
                "airport_code": "MNL",
                "country": "Philippines",
                "latlng": {
                    "lat": 14.599512,
                    "lng": 120.984219
                }
            },
            {
                "id": "6",
                "airport": "Mati",
                "airport_code": "DVO",
                "country": "Philippines",
                "latlng": {
                    "lat": 6.9438,
                    "lng": 126.2467
                }
            },
            {
                "id": "7",
                "airport": "Melbourne",
                "airport_code": "MEL",
                "country": "Australia",
                "latlng": {
                    "lat": -37.813628,
                    "lng": 144.963058
                }
            },
            {
                "id": "8",
                "airport": "Nanning",
                "airport_code": "NNG",
                "country": "China",
                "latlng": {
                    "lat": 22.6096,
                    "lng": 108.1717
                }
            },
            {
                "id": "9",
                "airport": "Narita",
                "airport_code": "NRT",
                "country": "Japan",
                "latlng": {
                    "lat": 42.7926,
                    "lng": 141.6705
                }
            },
            {
                "id": "10",
                "airport": "Singapore",
                "airport_code": "SIN",
                "country": "Singapore",
                "latlng": {
                    "lat": 1.352083,
                    "lng": 103.819836
                }
            }
        ];
        return results.map(res => {
            return {
                airport: res.airport,
                airport_code: res.airport_code,
                country: res.country,
                latLng: res.latlng,
                name: res.airport + ' ' + res.airport_code,
                search: res.airport_code.toLowerCase()
            }
        });
    }

    queryStates(query) {
        const results = query ? this.filterSearch(query) : this.states;
        const deferred = Q.get(this).defer();

        TIMEOUT.get(this)(() => {
            deferred.resolve(results);
        }, Math.random() * 1000, false);

        return deferred.promise;
    }

    filterSearch(query) {
        return this.states.filter((states) =>
            states.search.indexOf(angular.lowercase(query)) === 0
        );
    }


    /**
     * 
     * Find route using this Parameters
     */
    selectedOrigin(code) {
        this.MAP.origin = code ? this.filterCode(code) : '';
    }

    selectedDestination(code) {
        this.MAP.destination = code ? this.filterCode(code) : '';
    }

    selectedHub(code) {
        this.MAP.hub = code ? this.filterCode(code) : '';
    }

    processNodes() {
        if (this.MAP.hub !== '') {
            this.MAP.routes['selected'] = [[this.MAP.origin[0].latLng, this.MAP.hub[0].latLng, this.MAP.destination[0].latLng]];
        } else {
            this.MAP.routes['selected'] = [[this.MAP.origin[0].latLng, this.MAP.destination[0].latLng]];
            const hubs = this.filterHubs(this.MAP.origin[0].airport_code, this.MAP.destination[0].airport_code);

            let arry = [];
            let arry1 = [];
            angular.forEach(hubs, (val, key) => {
                arry.push([val.origin_latlng, val.hub_latlng, val.destination_latlng]);
                arry1.push({
                    "hub": val.hub,
                    "carrier_code": val.carrier_code,
                    "carrier_name": val.carrier_name
                });
            });
            this.MAP.routes['hubs'] = arry;
            this.MAP.hubsList = arry1;
        }
    }

    filterHubs(FROM, TO) {
        const states = [
            {
                "id": "1",
                "origin": "Manila",
                "origin_code": "MNL",
                "origin_latlng": {
                    "lat": 14.599512,
                    "lng": 120.984219
                },
                "destination": "Dubai",
                "destination_code": "DXB",
                "destination_latlng": {
                    "lat": 25.2048,
                    "lng": 55.2708
                },
                "hub": "Bangkok",
                "hub_code": "BKK",
                "hub_latlng": {
                    "lat": 15.8700,
                    "lng": 100.9925
                },
                "carrier_code": "QF",
                "carrier_name": "Qantas Airways"
            },
            {
                "id": "2",
                "origin": "Manila",
                "origin_code": "MNL",
                "origin_latlng": {
                    "lat": 14.599512,
                    "lng": 120.984219
                },
                "destination": "Dubai",
                "destination_code": "DXB",
                "destination_latlng": {
                    "lat": 25.2048,
                    "lng": 55.2708
                },
                "hub": "Hyderabad",
                "hub_code": "HYD",
                "hub_latlng": {
                    "lat": 17.3850,
                    "lng": 78.4867
                },
                "carrier_code": "EK",
                "carrier_name": "Emirates Airlines"
            },
            {
                "id": "3",
                "origin": "Manila",
                "origin_code": "MNL",
                "origin_latlng": {
                    "lat": 14.599512,
                    "lng": 120.984219
                },
                "destination": "Dubai",
                "destination_code": "DXB",
                "destination_latlng": {
                    "lat": 25.2048,
                    "lng": 55.2708
                },
                "hub": "Nanning",
                "hub_code": "NNG",
                "hub_latlng": {
                    "lat": 22.6096,
                    "lng": 108.1717
                },
                "carrier_code": "CA",
                "carrier_name": "Air China"
            },
            {
                "id": "4",
                "origin": "Manila",
                "origin_code": "MNL",
                "origin_latlng": {
                    "lat": 14.599512,
                    "lng": 120.984219
                },
                "destination": "Dubai",
                "destination_code": "DXB",
                "destination_latlng": {
                    "lat": 25.2048,
                    "lng": 55.2708
                },
                "hub": "Singapore",
                "hub_code": "SIN",
                "hub_latlng": {
                    "lat": 1.352083,
                    "lng": 103.819836
                },
                "carrier_code": "SQ",
                "carrier_name": "SIngapore Airlines"
            }
        ];

        return states.filter((state) =>
            (state.origin_code.indexOf(FROM) === 0 && state.destination_code.indexOf(TO) === 0)
        );
    }

    filterCode(val) {
        return this.states.filter((state) =>
            state.airport_code.indexOf(val) === 0
        );
    }
}