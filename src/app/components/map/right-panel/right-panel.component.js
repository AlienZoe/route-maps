/**
* Name: Side-Panel Component
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
import './right-panel.scss';
import template from './right-panel';
import dialog from './dialog';
import Base from './base';

const SIDENAV = new WeakMap();
const TIMEOUT = new WeakMap();
const DIALOG = new WeakMap();

class Controller extends Base {
    constructor($q, $timeout, $mdSidenav, $mdDialog, mapService) {
        super($q, $timeout, mapService);
        TIMEOUT.set(this, $timeout);
        SIDENAV.set(this, $mdSidenav);
        DIALOG.set(this, $mdDialog);

        this.noCache = false;
        this.searchOrigin = null;
        this.searchDestination = null;
        this.searchHub = null;
        this.action = "Find Route";
        this.showSpinner = false;
        this.diameter = 25;

        // this.origin = '';
        // this.destination = '';
        // this.hub = '';
        this.states = this.loadAll();
    }

    $onInit() {
        this.rightOpen = true;
    }

    findRoutes() {
        const that = this.MAP;
        this.showSpinner = true;

        TIMEOUT.get(this)(() => {
            this.showSpinner = false;
            that.routes = []; //This will reset to empty every action
            that.hubsList = [];

            this.processNodes();
            that.mapGroup.showRouteLine = that.routes;
        }, 2000);
    }

    showDialog(evt) {
        DIALOG.get(this).show({
            controller: ['$mdDialog', 'mapService', Dialog],
            controllerAs: 'vm',
            template: dialog,
            parent: angular.element(document.body),
            targetEvent: evt,
            clickOutsideToClose: true,
        });
    }

    toggleRight() {
        SIDENAV.get(this)('right').toggle();
        this.rightOpen = SIDENAV.get(this)('right').isOpen();
        this.showRight = false;

        if (!this.rightOpen) {
            TIMEOUT.get(this)(() => {
                this.showRight = true;
            }, 1000);
        }
    }
}

const SERVICE = new WeakMap();
class Dialog {
    constructor($mdDialog, mapService) {
        SERVICE.set(this, mapService);
        this._mdDialog = $mdDialog;

        this.rowSelection = false;
        this.multiSelect = false;
        this.autoSelect = true;
        this.decapitate = false;
        this.largeEditDialog = false;
        this.boundaryLinks = false;
        this.limitSelect = true;
        this.pageSelect = true;
        this.selected = [];
        this.limit = 5; //Limit of the items in a table
        this.page = 1; //Default paginate page
        this.initializeTable();
    }

    initializeTable() {
        SERVICE.get(this).loadHub().then(res => {
            this.states = res;
            this.limitOptions = [5, 10, 15, {
                label: 'All',
                value: function () {
                    return res.length ? res.length : 0;
                }
            }];
        });
    }

    Cancel() {
        this._mdDialog.cancel();
    }

    Answer(answer) {
        this._mdDialog.hide(answer);
    }
}

export const MapRightPanel = {
    template,
    require: {
        MAP: '^mapCanvas'
    },
    controller: ['$q', '$timeout', '$mdSidenav', '$mdDialog', 'mapService', Controller],
    controllerAs: 'vm',
    transclude: true
};