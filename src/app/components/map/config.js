/**
* Name: Map Config
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
import { Home } from './states';

export function config($uiRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    
    const $urlService    = $uiRouterProvider.urlService;
    const $stateRegistry = $uiRouterProvider.stateRegistry;
    
    //If no url found redirect to home
    $urlService.rules.otherwise({state: 'Home'});

    //Register all the States
    $stateRegistry.register(Home);
}