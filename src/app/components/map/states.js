/**
* Name: Map States
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/


//Home State
export const Home = {
    name: 'Home',
    url: '/',
    component: 'mapCanvas'
}
