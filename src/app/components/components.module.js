/**
* Name: App Component Module
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
import MapModule from './map';

const moduleName = 'appcomponent';

angular.module(moduleName, [
    MapModule
]);

export default moduleName;