/**
* Name: App Module
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
// import uiRouter from 'angular-ui-router';
// import '../assets/scss/global.scss';

// import components from './components/components.module';
// import { AppComponent } from './app.component';

// let mainModule = 'routemap';

// angular.module(mainModule, [
//     'uiRouter',
//     components,
//     'ngMaterial'
// ])
// .component('rmApp', AppComponent)
// .config(function($mdIconProvider) {
//     'ngInject';

//     $mdIconProvider.defaultIconSet('/src/lib/angular-material/css/mdi.svg');
// })
// .run(() => {
//     console.log(`Running Route Map Application...`);
//     console.log(`Powered by: ALIENZOE`);
// });

// export default mainModule;

import '../assets/scss/global.scss';

import Components from './components/components.module';
import { AppComponent } from './app.component';

let mainModule = 'routemap';

angular.module(mainModule, [
    'ui.router',
    Components,
    'ngMaterial',
    'md.data.table'
])
.component('rmApp', AppComponent)
.config(['$mdIconProvider', function($mdIconProvider) {
    $mdIconProvider.defaultIconSet('/public/mdi.svg');
}])
.run(() => {
    console.log(`Running Route Map Application...`);
    console.log(`Powered by: ALIENZOE`);
});

export default mainModule;