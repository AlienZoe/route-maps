/**
* Name: Main Module Bootstrap
*
* Version: 1.0.0
*
* Author: Joy Laurence M. Sangalang
*         pro.jls04@gmail.com
*         @ALIENZOE
*/
import { default as app } from './app.module';

angular.element( document )
       .ready( function() {
            let body = document.getElementsByTagName("body")[0];
            angular.bootstrap(body, [app]);
        });


//For Development HMR (HotModuleReplacement) run into web browser or client server.
if(process.env.NODE_ENV === 'development') {
    if(module.hot) {
        module.hot.accept();
    }
}